import os
import discord
import json
from discord.ext import commands

class admin(commands.Cog):
    def __init__(self,bot):
        self.bot = bot

    @commands.command()
    @commands.is_owner()
    async def updateAndReload(self,ctx):
        await self.updateBot(ctx)
        with open("conf.json") as conf:
            try:
                config = json.load(conf)
            except Exception as e:
                print("there was an error loading the json file ",e)
                await ctx.send("the json file is corrupted")
                return
        try:
            for ext in config["extension"]:
                await self.reload(ctx,ext)
        except KeyError as e:
            print("json missing config ",e)
            await ctx.send("the json file is missing config")

    @commands.command()
    @commands.is_owner()
    async def shutdown(self,ctx):
        await ctx.bot.logout()

    @commands.command()
    @commands.is_owner()
    async def restart(self,ctx):
        await self.restartBot(ctx)

    @commands.command()
    @commands.is_owner()
    async def updateAndRestart(self,ctx):
        await self.updateBot(ctx)
        await self.restartBot(ctx)

    @commands.command()
    @commands.is_owner()
    async def load(self,ctx,extName : str):
        try:
            self.bot.load_extension(extName)
        except (AttributeError, ImportError) as e:
            print("Error ",e)
            await ctx.send("```py\n{}: {}\n```".format(type(e).__name__, str(e)))
            return
        await ctx.send("{} is now loaded".format(extName))

    @commands.command()
    @commands.is_owner()
    async def unload(self,ctx,extName : str):
        try:
            self.bot.unload_extension(extName)
        except discord.ext.commands.errors.ExtensionNotLoaded as e:
            print("Error ",e)
            await ctx.send("{} is not loaded".format(extName))
            return
        await ctx.send("{} is now unloaded.".format(extName))

    @commands.command()
    @commands.is_owner()
    async def reload(self,ctx,extName : str):
        await self.unload(ctx,extName)
        await self.load(ctx,extName)

    @commands.command()
    @commands.is_owner()
    async def update(self,ctx):
        await self.updateBot(ctx)

    async def updateBot(self,ctx):
        await ctx.send("I'm updating")
        os.system("git pull")
        await ctx.send("I'm now up to date")

    async def restartBot(self,ctx):
        await ctx.send("I will now restart")
        os.system("systemctl restart gerhard.service")
        await ctx.bot.logout()

def setup(bot):
    bot.add_cog(admin(bot))
