import json
import sys
import io
import asyncpg
import asyncio

class eliteDB():
    def __init__(self):
        #load conf
        try:
            with open("conf.json") as conf :
                config = json.load(conf)
        except FileNotFoundError as e:
            print("Configuration file not found ",e)
            sys.exit()
        except json.decoder.JSONDecodeError as e:
            print("Configuration file is unreadable ",e)
            sys.exit()

        self.constr = "postgresql://" +  config["postgres"]['user'] + ":" + config["postgres"]['pass']
        self.constr += "@" + config["postgres"]['host'] + ":" +  str(config["postgres"]['port']) + "/" + config["postgres"]['database']

    async def connect(self):
        try:
            connection = await asyncpg.connect(self.constr)
        except Exception as e:
            print("Error while connecting to PostgreSQL", e)
        return connection

    async def getOverlap(self,system):
        #connect to db
        connection = await self.connect()

        if not await self.checkExist(system):
            #search for system with a name close to the one provided
            system = await self.findClosest(system)
            if not system:
                #nothing found
                return False

        sysId =  await self.getSysId(system)

        sql = "SELECT h.\"bodyName\",h.commodity,h.\"overlaps\" FROM elitetracker.overlaps h "
        sql += "WHERE h.\"systemID64\"=$1 ORDER BY 3 DESC"

        stmt = await connection.prepare(sql)
        result = await stmt.fetch(sysId)

        if len(result) == 0:
            return None

        await connection.close()
        return result

    async def getNearOverlap(self,commodity,system,overlap):
        connection = await self.connect()
        #check that system exist
        if not await self.checkExist(system):
            #search for system with a name close to the one provided
            system = await self.findClosest(system)
            if not system:
                #nothing found
                return False,False

        sqlPrep = "SET search_path TO elite, \"$user\", public;"
        sql = """SELECT
            a.name,
            a.coords::cube<->(SELECT coords::cube FROM elite.systems WHERE lower(name) = lower($1) limit 1) AS dist,
            b."bodyName",
            b.overlaps
        FROM
            elite.systems a
        JOIN
            elitetracker.overlaps b
        ON
            a.id64 = b."systemID64"
        WHERE
            b.commodity = $2
        AND
            b.overlaps >= $3
        ORDER BY 2,4 LIMIT 8;
        """

        await connection.execute(sqlPrep)
        stmt = await connection.prepare(sql)

        result = await stmt.fetch(system,commodity,overlap)
        await connection.close()
        return result,system

    async def getHotSpots(self,system):
        #connect to db
        connection = await self.connect()

        if not await self.checkExist(system):
            #search for system with a name close to the one provided
            system = await self.findClosest(system)
            if not system:
                #nothing found
                return False

        sysId =  await self.getSysId(system)

        sql = """SELECT body_name,string_agg(  h.signal_count || ' ' || h.signal_type,'\n' ORDER BY h.signal_count DESC ) as typ,p.distance FROM elite.hotspots h
        JOIN elite.planets p ON h.planet_id64 = p.id
        WHERE h.sys_id64=$1 AND h.signal_type NOT LIKE '$SAA%'
        GROUP BY 1,3
        ORDER BY 3
        """ 
        stmt = await connection.prepare(sql)
        result = await stmt.fetch(sysId)

        if len(result) == 0:
            return None
        await connection.close()
        return result

    async def getDetail(self,system,full):
        #connect to db
        connection = await self.connect()

        #check that system exist
        if not  await self.checkExist(system):
            #search for system with a name close to the one provided
            system =  await self.findClosest(system)
            if not system:
                #nothing found
                return False


        sysId =  await self.getSysId(system)

        sql = "SELECT * FROM elite.planets WHERE sys_id64=$1"
        if not full:
            sql += " AND has_ring = true"
        sql += " ORDER BY distance"

        stmt = await connection.prepare(sql)
        result = await stmt.fetch(sysId)

        #in case system has no body
        if len(result) == 0:
            return None

        await connection.close()
        return result

    async def getSys(self,name,type="M",pristine=True):
        connection = await self.connect()
        system = name
        #check that system exist
        if not await self.checkExist(name):
            #search for system with a name close to the one provided
            system = await self.findClosest(name)
            if not system:
                #nothing found
                return False,False

        sqlPrep = "SET search_path TO elite, \"$user\", public;"
        sql = """
            select
                a.name,
                a.coords::cube<->(select coords::cube from elite.systems where lower(name) = lower($1) limit 1) as dist
            from
                elite.systems a
            where """

        if type == "M":
            sql += " a.metallic = true "
        elif type == "I":
            sql += " a.icy = true "
        elif type == "R":
            sql += " a.rocky = true "
        else:
            sql += " a.metal_rich = true "
        if pristine:
            sql += " and a.reserve = 'Pristine' "
        sql += " order BY 2 limit 6;"

        await connection.execute(sqlPrep)
        stmt = await connection.prepare(sql)
        result = await stmt.fetch(system)

        await connection.close()

        #return result and system name to warn user if another close system was used
        return result,system

    #find a system with a name close to the one provided
    #this function is DB intensive, expect 10+ sec execution
    async def findClosest(self,name):
        connection = await self.connect()

        sql = "SELECT name FROM elite.systems WHERE lower(name) LIKE lower($1) LIMIT 1"
        name = name+"%"

        stmt = await connection.prepare(sql)
        result = await stmt.fetch(name)

        if len(result) == 0:
            return False

        await connection.close()
        return result[0][0]

    #get the id64 of a system name
    async def getSysId(self,name):
        connection = await self.connect()
        sql = "SELECT id64 FROM elite.systems WHERE lower(name) = lower($1) LIMIT 1"

        stmt = await connection.prepare(sql)
        result = await stmt.fetch(name)

        #check that we have something just in case
        if len(result) == 0:
            return False

        await connection.close()
        return result[0][0]

    async def getNearHotspots(self,commodity,system):
        connection = await self.connect()
        #check that system exist
        if not await self.checkExist(system):
            #search for system with a name close to the one provided
            system = await self.findClosest(system)
            if not system:
                #nothing found
                return False,False
        sqlPrep = "SET search_path TO elite, \"$user\", public;"
        sql = """SELECT
            a.name,
            a.coords::cube<->(SELECT coords::cube FROM elite.systems WHERE lower(name) = lower($1) limit 1) AS dist,
            b.signal_count,
            b.body_name,
            c.distance
        FROM
            elite.systems a
        JOIN
            elite.hotspots b
        ON
            a.id64 = b.sys_id64
        JOIN
	       elite.planets c
        ON
	       c.id = b.planet_id64
        WHERE
        	b.signal_type = $2

        ORDER BY 2,5 LIMIT 8;
        """

        await connection.execute(sqlPrep)
        stmt = await connection.prepare(sql)

        result = await stmt.fetch(system,commodity)
        await connection.close()
        return result,system

    async def getHotSpotCommodity(self):
        connection = await self.connect()
        sql = "SELECT DISTINCT signal_type AS c FROM elite.hotspots"

        result = await connection.fetch(sql)
        await connection.close()
        return result

    #true or false if system exist in DB or not
    async def checkExist(self,name):
        return False
        """
        connection = await self.connect()

        sql = "SELECT count(*) FROM elite.systems WHERE lower(name) = lower($1) LIMIT 1"

        stmt = await connection.prepare(sql)
        result = await stmt.fetch(name)

        await connection.close()
        if result[0][0] == 1:
            return True
        else:
            return False
        """
