def translateCommodity(commodity):
    commodity = commodity.capitalize()
    search = commodity.lower()
    table = {
        "ltd" : "LowTemperatureDiamond",
        "ldt" : "LowTemperatureDiamond",
        "diamond" : "LowTemperatureDiamond",
        "diamonds" : "LowTemperatureDiamond",
        "lowtemperaturediamond" : "LowTemperatureDiamond",
        "vopal" : "Opal",
        "vopals" : "Opal",
        "opals" : "Opal",
        "vo" : "Opal",
        "o" : "Opal",
        "pain" : "Painite",
        "p" : "Painite",
        "bromellite" : "Bromellite",
        "bro" : "Bromellite"
    }
    if search in table:
        commodity = table[search]
    return commodity

#this function is just here to restrict the bot channel access in imu
async def imuCheck(ctx):
    if ctx.message.guild.id == 398943203463659520:
        if ctx.message.channel.id == 433342103485939722 or ctx.message.channel.id == 531429052511420418:
            return True
        else:
            return False
    else :
        return True
