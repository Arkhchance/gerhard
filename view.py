import discord
import asyncio
from discord.ext import commands
from database import eliteDB

def boolToStr(val):
    if val:
        return "Yes"
    else:
        return "No"

class view(commands.Cog):
    def __init__(self,bot):
        self.bot = bot
        self.db = eliteDB()

    @commands.cooldown(1, 20, commands.BucketType.user)
    @commands.command()
    async def Detail(self,ctx,*,arg):
        await self.getDetail(ctx,arg)

    @commands.cooldown(1, 40, commands.BucketType.user)
    @commands.command()
    async def FullDetail(self,ctx,*,arg):
        await self.getDetail(ctx,arg,True)

    #get bodies detail of a system, only ring of full system
    async def getDetail(self,ctx,system,full=False):
        await ctx.send("I'm searching ..")
        planets = await self.db.getDetail(system,full)

        if planets == False:
            await ctx.send(ctx.author.mention + " I didn't find a system called " + system)
            return
        if planets == None:
            await ctx.send(ctx.author.mention + " There is no known planets with rings in " + system)
            return

        await ctx.send("check your PM " + ctx.author.mention)

        for planet in planets:
            message=discord.Embed(title=planet["name"], color=0x0700c9)
            message.add_field(name="Planet class",value=str(planet["planet_class"]), inline=True)
            message.add_field(name="Distance from star",value=str(planet["distance"])+" Ls", inline=True)
            if planet["has_ring"]:
                reserve = "none" if planet["reserve"] == "" else planet["reserve"]

                message.add_field(name="\u200B",value="\u200B", inline=True)
                message.add_field(name="Ring type",value=str(planet["ring_type"]), inline=True)
                message.add_field(name="Reserve",value=reserve, inline=True)
                message.add_field(name="\u200B",value="\u200B", inline=True)

            message.add_field(name="Landable :",value=boolToStr(planet["landable"]), inline=True)
            message.add_field(name="Axial Tilt",value=str(planet["axial_tilt"])+"°", inline=True)
            message.add_field(name="Gravity",value="{:.2f}".format(planet["gravity"])+" g", inline=True)
            message.add_field(name="Earth mass",value="{:.2f}".format(planet["earth_masses"])+" em", inline=True)
            message.add_field(name="Radius",value="{:.2f}".format(planet["radius"]/1000)+" Km", inline=True)
            message.add_field(name="Temerature",value="{:.2f}".format(planet["temperature"])+" K", inline=True)
            message.add_field(name="Pressure",value="{:.2f}".format(planet["pressure"])+" Atmospheres", inline=True)
            message.add_field(name="Orbital period",value="{:.2f}".format(planet["orbital_period"])+" d", inline=True)
            message.add_field(name="Semi major axis",value="{:.2f}".format(planet["semi_major_axis"])+" au", inline=True)
            message.add_field(name="Orbital eccentricity",value="{:.2f}".format(planet["eccentricity"]), inline=True)
            message.add_field(name="Orbital inclination",value="{:.2f}".format(planet["inclination"])+"°", inline=True)
            message.add_field(name="Argument of periapsis",value="{:.2f}".format(planet["arg_periapsis"])+"°", inline=True)
            message.add_field(name="Rotational period",value="{:.2f}".format(planet["rotational_period"])+" d", inline=True)
            message.add_field(name="Orbital eccentricity",value="{:.2f}".format(planet["eccentricity"]), inline=True)

            message.add_field(name="Tidally locked :",value=boolToStr(planet["tidally_locked"]), inline=True)

            if planet["terraforming"] != "":
                message.add_field(name="Terraforming state",value=planet["terraforming"], inline=True)
            if planet["volcanism"] != "":
                message.add_field(name="Volcanism",value=str(planet["volcanism"]), inline=True)
            if planet["atmosphere"] != "":
                message.add_field(name="Atmosphere",value=planet["atmosphere"], inline=True)
            if planet["atmosphere_composition"] != "":
                message.add_field(name="\u200B",value="\u200B", inline=False)
                message.add_field(name="Atmosphere composition",value=planet["atmosphere_composition"], inline=True)

            await ctx.author.send(embed=message)
            # 500 ms between msg
            await asyncio.sleep(0.5)

def setup(bot):
    bot.add_cog(view(bot))
