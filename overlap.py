import discord
import time
from discord.ext import commands
from database import eliteDB
from helper import translateCommodity
from helper import imuCheck

class overlap(commands.Cog):
    def __init__(self,bot):
        self.bot = bot
        self.db = eliteDB()

    @commands.command()
    async def Overlap(self,ctx,*,arg):
        if not await imuCheck(ctx) :
            await ctx.send("Please use my commands in <#433342103485939722>")
            return
        startTime = time.time()
        system = arg
        async with ctx.channel.typing():
            overlaps = await self.db.getOverlap(system)
            if overlaps == False:
                return await ctx.send(ctx.author.mention + " I didn't find a system called " + system)
            if overlaps == None:
                return await ctx.send(ctx.author.mention + " There is no known overlap in " + system)

            message=discord.Embed(title="Overlaps in "+system, color=0xad1711)
            for overlap in overlaps:
                message.add_field(name="Planet",value=overlap["bodyName"], inline=True)
                message.add_field(name="Commodity",value=overlap["commodity"], inline=True)
                message.add_field(name="Overlap",value=str(overlap["overlaps"]), inline=True)

            stopTime = time.time()
            message.set_footer(text="generated in {:.2f}sec".format(stopTime - startTime))
            await ctx.send(embed=message)

    @commands.command()
    async def NearOverlap(self,ctx,*,arg):
        if not await imuCheck(ctx) :
            await ctx.send("Please use my commands in <#433342103485939722>")
            return
        startTime = time.time()
        async with ctx.channel.typing():
            try:
                comm,overlap,system = arg.split(maxsplit=2)
                overlap = int(overlap)
            except ValueError:
                try:
                    comm,system = arg.split(maxsplit=1)
                    overlap = 0
                except ValueError:
                    await ctx.send(ctx.author.mention + " I need more arguments : `$NearOverlap <commodity> <amount> <system>` ")
                    return

            if system.isnumeric():
                await ctx.send(ctx.author.mention + " You need to provide me a system : `$NearOverlap <commodity> <amount> <system>` \nex : `$NearOverlap ltd 2 Sol`")
                return

            comm = translateCommodity(comm)
            result,sysName = await self.db.getNearOverlap(comm,system,overlap)

            if result == False:
                return await ctx.send(ctx.author.mention + " I didn't find a system called " + system)
            if len(result) == 0:
                return await ctx.send(ctx.author.mention + " No result ¯\_(ツ)_/¯ \nDid you make a typo ? Write `$CommoditiesList` to see what I'm tracking")

            message=discord.Embed(title=comm + " overlap near " + sysName, color=0x9217e3)

            for sys in result:
                plural = "s" if sys["overlaps"] > 1 else ""
                message.add_field(name="System",value=sys["name"] + "\n{:.2f} Ly".format(sys["dist"]), inline=True)
                message.add_field(name="Planet",value=sys["bodyName"], inline=True)
                message.add_field(name=comm,value=str(sys["overlaps"])+ " overlap" + plural, inline=True)


            message.add_field(name="For more detail",value="et!overlaps system <system>",inline=False)
            stopTime = time.time()
            message.set_footer(text="generated in {:.2f}sec".format(stopTime - startTime))
            await ctx.send(embed=message)

    @commands.command()
    async def CommoditiesList(self,ctx):
        async with ctx.channel.typing():
            commodity = await self.db.getHotSpotCommodity()
            msg = ' - '.join(c["c"] for c in commodity)
            st = "\n```"
            await ctx.send("Hotspots' commodities I'm tracking : " + st + msg + st)


def setup(bot):
    bot.add_cog(overlap(bot))
