#!/usr/bin/python3.6
import json
import discord
from discord.ext import commands
from discord.ext.commands import CommandNotFound


#load conf
with open("conf.json") as conf:
    data = json.load(conf)
    token = data["token"]

bot = commands.Bot(command_prefix="$",case_insensitive=True)
bot.remove_command("help")

for ext in data["extension"]:
    try:
        bot.load_extension(ext)
    except Exception as e:
        exc = "{}: {}".format(type(e).__name__, e)
        print("Failed to load extension {}\n{}".format(ext, exc))

@bot.event
async def on_command_error(ctx, error):
    if isinstance(error, CommandNotFound):
        return await ctx.send(ctx.author.mention + " Command unknown, use `$help` if necessary")
    raise error

@bot.event
async def on_ready():
    print("Logged in as")
    print(bot.user.name)
    print(bot.user.id)
    print("------")
    await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.playing,name="$help"))

bot.run(token)
