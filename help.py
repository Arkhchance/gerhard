import discord
from discord.ext import commands

class help(commands.Cog):
    def __init__(self,bot):
        self.bot = bot
        self.helpMsg = """
```
My commands are case-insensitive

$Help <command>                     Get detailed help about a command

-------------------------------------------------------------------------------------------

$Icy <system>                       Get closest systems with pristine icy rings
$Metallic <system>                  Get closest systems with pristine metallic rings
$Rocky <system>                     Get closest systems with pristine rocky rings
$MetalRich <system>                 Get closest systems with pristine metal rich rings

-------------------------------------------------------------------------------------------

$CommoditiesList                            Get a list of hotspot's commodities tracked
$Hotspots <system>                          Get hotspots in system
$NearHotspots <commodity> <system>          Get hotspots with commotidy near system
$Overlap <system>                           Get reported overlap in system
$NearOverlap <commodity> <amount> <system>  Get reported commodity's overlap near system

-------------------------------------------------------------------------------------------

$Detail <system>                    Get a detailed view of a system's rings (20sec cooldown)(in private message)
$FullDetail <system>                Get a detailed view of a system's body (40sec cooldown)(in private message)

Find my source code at https://gitlab.com/Arkhchance/gerhard
```
"""
        self.helpRings = """
```
${0} <system>

This command will list you the 6 closest systems with {0} rings (pristine reserve only)

Example :
${0} Sol
${0} Colonia

You can add All in front of the command to remove the pristine restriction

Example :
$All{0} Sol
$All{0} Borann
```
"""
        self.helpHotspots = """
```
$Hotspots <system>

This command will list you all the hotspots inside a given system, planet by planet
They may or may not be overlaps !

example :
$Hotspots Rodentia
$Hotspots Dubbuennel
```
"""
        self.helpNearHotspots = """
```
$NearHotspots <commodity> <system>

This command will list you the closest systems with a commodity's hotspot

To get a list of commodity, ask me with : $CommoditiesList
They may or may not be overlaps !
You can use LTD for Low Temperature Diamond or VO for Void Opal etc..

Example :
$NearHotspots Grandidierite Sol
$NearHotspots Tritium Colonia
$NearHotspots LTD Borann
```
"""
        self.helpOverlap = """
```
$Overlap <system>

This command will list you all reported overlaps inside the given system commodity by commodity

Example :
$Overlap Eol Prou NH-K C9-15
```
"""
        self.helpNearOverlap = """
```
$NearOverlap <commodity> <amount> <system>

This command will list you the closest system with reported overlap of commodity > or = to amount
To get a list of commodity, ask me with : $CommoditiesList
amount is optional but works as folow :
    - 1 is overlap with a RES site
    - 2 is double overlap
    - 3 triple overlap (rare)

Example :
$NearOverlap Tritium Sol
$NearOverlap Tritium 2 Colonia
$NearOverlap LTD 3 Borann
```
"""
        self.helpDetail = """
```
$Detail <system>

This command will provide you a detail view of ring planet in a system
Due to the amount of result this command can get, I will answer you in private message
This command has a 20 seconds cooldown

Example:
$Detail Sol
$Detail Colonia
```
"""
        self.helpFullDetail = """
```
$FullDetail <system>

This command will provide you a detail view of all planets in a system
Due to the amount of result this command can get, I will answer you in private message
This command has a 40 seconds cooldown

Example:
$FullDetail Sol
$FullDetail Colonia

```
"""

    @commands.command()
    async def help(self,ctx,*args):
        if len(args) == 0:
            await ctx.send(self.helpMsg)
        else:
            arg = args[0].lower()
            if "icy" == arg:
                await ctx.send(self.helpRings.format("Icy"))
            elif "metallic" == arg:
                await ctx.send(self.helpRings.format("Metallic"))
            elif "rocky" == arg:
                await ctx.send(self.helpRings.format("Rocky"))
            elif "metalrich" == arg:
                await ctx.send(self.helpRings.format("MetalRich"))
            elif "commoditieslist" == arg:
                await ctx.send("`This command will list you the commodities I'm tracking in hotspots`")
            elif "hotspots" == arg:
                await ctx.send(self.helpHotspots)
            elif "nearhotspots" == arg:
                await ctx.send(self.helpNearHotspots)
            elif "overlap" == arg:
                await ctx.send(self.helpOverlap)
            elif "nearoverlap" == arg:
                await ctx.send(self.helpNearOverlap)
            elif "detail" == arg:
                await ctx.send(self.helpDetail)
            elif "fulldetail" == arg:
                await ctx.send(self.helpFullDetail)
            else:
                await ctx.send(" I don't have a command named "+ str(arg) + " "+ ctx.author.mention)

def setup(bot):
    bot.add_cog(help(bot))
