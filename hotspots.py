import discord
import time
from discord.ext import commands
from database import eliteDB
from helper import translateCommodity
from helper import imuCheck

class hotspots(commands.Cog):
    def __init__(self,bot):
        self.bot = bot
        self.db = eliteDB()

    @commands.command()
    async def Hotspots(self,ctx,*,arg):
        if not await imuCheck(ctx) :
            await ctx.send("Please use my commands in <#433342103485939722>")
            return

        startTime = time.time()
        system = arg
        async with ctx.channel.typing():
            hotspots = await self.db.getHotSpots(system)
            if hotspots == False:
                return await ctx.send(ctx.author.mention + " I didn't find a system called " + system)

            if hotspots == None:
                return await ctx.send(ctx.author.mention + " There is no known hotspots in " + system)


            message = discord.Embed(title="Hotspots in "+system, color=0x10ba00)

            for hotspot in hotspots:
                message.add_field(name="Planet",value=hotspot["body_name"], inline=True)
                message.add_field(name="Distance from star",value=hotspot["distance"], inline=True)
                message.add_field(name="Hotspot",value=hotspot["typ"], inline=False)

            stopTime = time.time()
            message.set_footer(text="generated in {:.2f}sec".format(stopTime - startTime))
            await ctx.send(embed=message)

    @commands.command()
    async def NearHotspots(self,ctx,*,arg):
        if not await imuCheck(ctx) :
            await ctx.send("Please use my commands in <#433342103485939722>")
            return
        startTime = time.time()
        async with ctx.channel.typing():
            comm,system = arg.split(maxsplit=1)
            comm = translateCommodity(comm)

            result,sysName = await self.db.getNearHotspots(comm,system)
            if result == False:
                return await ctx.send(ctx.author.mention + " I didn't find a system called " + system)
            if len(result) == 0:
                return await ctx.send(ctx.author.mention + " No commodities named " + comm)

            message=discord.Embed(title=comm +" hotspots near " + sysName, color=0x9217e3)

            for planet in result:
                plural = "s" if planet["signal_count"] > 1 else ""
                message.add_field(name="System",value=planet["name"] + "\n{:.2f} Ly".format(planet["dist"]), inline=True)
                message.add_field(name="Planet",value=planet["body_name"]+ "\n{} Ls".format(planet["distance"]), inline=True)
                message.add_field(name=comm,value=str(planet["signal_count"])+ " hotspot" + plural, inline=True)


            stopTime = time.time()
            message.set_footer(text="generated in {:.2f}sec".format(stopTime - startTime))
            await ctx.send(embed=message)

def setup(bot):
    bot.add_cog(hotspots(bot))
