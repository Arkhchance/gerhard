import discord
import time
from discord.ext import commands
from database import eliteDB


class verity(commands.Cog):
    def __init__(self,bot):
        self.bot = bot
        self.db = eliteDB()

    #get result from DB and print it
    #type = ring type (M/I/R)
    #name = ring type name
    async def getResult(self,ctx,system,type,name,pristine=True):
        startTime = time.time()
        async with ctx.channel.typing():
            systems,sysname = await self.db.getSys(system,type,pristine)

            if not systems:
                await ctx.send(ctx.author.mention + " I didn't find a system called " + system)
                return

            title = "Closest "
            if pristine:
                title += "pristine "
            title += "known system with "+ name + " rings close to " + sysname

            message=discord.Embed(title=title, color=0x9217e3)
            if sysname.lower() != system.lower() :
                msg = " I haven't found a system called " + system  + " but I found a close one : " + sysname
                await ctx.send(ctx.author.mention + msg)

            for system in systems:
                head = str(system[0])
                msg  = "{:.2f}".format(system[1]) + " Ly"
                message.add_field(name=head, value=msg, inline=False)
            stopTime = time.time()
            message.set_footer(text="generated in {:.2f}sec".format(stopTime - startTime))
            await ctx.send(embed=message)

    @commands.command()
    async def Metallic(self,ctx,*,arg):
        await self.getResult(ctx,arg,"M","metallic")
    @commands.command()
    async def AllMetallic(self,ctx,*,arg):
        await self.getResult(ctx,arg,"M","metallic",False)

    @commands.command()
    async def Icy(self,ctx,*,arg):
        await self.getResult(ctx,arg,"I","icy")
    @commands.command()
    async def AllIcy(self,ctx,*,arg):
        await self.getResult(ctx,arg,"I","icy",False)

    @commands.command()
    async def Rocky(self,ctx,*,arg):
        await self.getResult(ctx,arg,"R","rocky")
    @commands.command()
    async def AllRocky(self,ctx,*,arg):
        await self.getResult(ctx,arg,"R","rocky",False)

    @commands.command()
    async def MetalRich(self,ctx,*,arg):
        await self.getResult(ctx,arg,"MR","metal rich")
    @commands.command()
    async def AllMetalRich(self,ctx,*,arg):
        await self.getResult(ctx,arg,"MR","metal rich",False)

def setup(bot):
    bot.add_cog(verity(bot))
